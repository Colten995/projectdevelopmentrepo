Mandatory Programs:
Android Studio,
Gradle installation,
Ionic Framework

How to Build and Run The FishTracker Project

	1. Navigate to the source code page by clicking the Source link under ProjectDevelopmentRepo in the top left of the web page
	2. Click the three dots symbol in the top right of the web page and click download repository from the drop-down
	3. After the download finishes navigate to the downloads folder on your computer
	4. Right-click on the repository folder you just downloaded and extract the files to a desired location
	5. Open the command prompt and navigate to the files you just extracted
	6. Go to "Colten995-projectdevelopmentrepo-1e43b6cbe953\FishTracker\MyApplication"
	7. execute the command "ionic build"
	8. After that command finishes execute the command "ionic cordova run android"
	9. Android Studio will open and the app will attempt to run on an android virtual device in Android Studio

How To Use The FishTracker App

	Add Fish
	
		This page is used to add fish to your collection. You can enter different information for the fish such as:
		
		- Species
		- Name
		- Weight
		- Length
		- Location
		- Picture
		- Date
		
		Click the "Add To Collection" button at the bottom to add the fish when your are done
		
	View Fish
	
		This page is used to view the fish in your collection. You can tap on different fish to view more information about them.
		
	Settings
	
		On this page you can change different settings in your app. You can change the units you measure weight in, the measurement 
		system for measuring your fishes length. You can decide whether or not your collection will be deleted on app startup. To 
		save settings you can click the "Save Settings" button and to clear the database you can click the "Clear Database" button.

Reason For License (License.md)

	I chose to use this license because it is a short license and my project is an open-source software. The license is short, so it puts 
	very little limitation on the uses of the software. This project was made purely by myself, so I feel like I don't need to place any 
	limitations on anyone using the software. The software has no need to be private I am not afraid of anyone else taking my ideas or 
	using the sofwtare for their own gain, monetary or otherwise. The software is available to anyone, therefore it is open-source. 
	If anybody wanted to use the software I would encourage them to. I would invite anyone to help make this project better if they had any 
	ideas. I encourage others to help improve this app. I feel like there is nothing I am liable for with this software, However if there 
	are any damages or the like caused by using this software I will not be liable for them with this license.

	Change made